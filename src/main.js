import './css/main.scss';

const speech = document.querySelector('.speech');
const intent = document.querySelector('.intent');
const myVideo = document.querySelector('.cams-dave > video');
const userVideo = document.querySelector('.cams-user > video');
const splashScreen = document.querySelector('.splash');
const startButton = document.querySelector('.splash-start');
const constraints = { audio: true, video: { width: 720, height: 1280 } };
const intents = require('./intents.json');

let recognition;
let idling = true;

const env = window.location.href.indexOf('localhost') > 0 ? 'dev' : 'prod';

const nlpURL = env === 'dev'
  ? 'http://localhost:5000/the-interview-app/us-central1/api/nlp'
  : 'https://us-central1-the-interview-app.cloudfunctions.net/api/nlp';


const getIntent = text => new Promise((resolve) => {
  console.log('getIntent', text);
  intent.value = 'sending speech to api.ai for analysis';
  fetch(`${nlpURL}/${text}`)
    .then(response => response.json())
    .then((response) => {
      intent.value = `intent: ${response.action}`;
      resolve(response.action);
    });
});

const random = array => array[Math.floor(Math.random() * array.length)];

const respondToUser = (response) => {
  const userIntent = intents[response];
  idling = false;

  if (!userIntent) return playVideo(intents.fallback);

  return playVideo(random(userIntent));
};

const startListening = () => {
  console.log('startListening');

  recognition = new window.webkitSpeechRecognition();

  recognition.onresult = (e) => {
    if (!idling) return;

    let text = '';
    for (let i = e.resultIndex; i < e.results.length; i += 1) {
      text += e.results[i][0].transcript;
    }
    speech.value = text;
    getIntent(text).then(respondToUser);
  };

  recognition.onEnd = () => {
    setTimeout(startListening, 250);
  };
  recognition.lang = 'en-US';
  recognition.start();
};

const stopListening = () => {
  if (recognition) recognition.stop();
};

const playVideo = (video) => {
  myVideo.src = `videos/${video}.mp4`;
  idling = video.indexOf('idle') >= 0;
  stopListening();
};

const videoComplete = () => {
  startListening();
  playVideo(random(intents.idle));
};


myVideo.addEventListener('ended', videoComplete);

const getCamera = () => new Promise((resolve) => {
  navigator.mediaDevices.getUserMedia(constraints)
    .then((stream) => {
      userVideo.srcObject = stream;
      userVideo.onloadedmetadata = () => {
        userVideo.play();
        return resolve(true);
      };
    })
    .catch((err) => {
      console.log(`${err.name}: ${err.message}`);
      return resolve(false);
    });
});


const start = () => {
  splashScreen.classList.add('hidden');
  startButton.removeEventListener('click', start);
  getCamera().then(startListening);
};

startButton.addEventListener('click', start);

document.addEventListener('DOMContentLoaded', () => {
  setTimeout(() => { playVideo(random(intents.idle)); }, 1000);
});
